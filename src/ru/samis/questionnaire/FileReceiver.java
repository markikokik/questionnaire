package ru.samis.questionnaire;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ru.samis.geoclusters.Main;
import ru.samis.geoclusters.entities.Cluster;
import ru.samis.geoclusters.entities.ClustersWithTimeline;
import ru.samis.geoclusters.entities.Location;

@WebServlet(name = "recv", urlPatterns = {"/androUpload"})
public class FileReceiver extends HttpServlet {

    protected void doPost(HttpServletRequest request,
                          HttpServletResponse response)
            throws ServletException, IOException {
        ServletInputStream in = request.getInputStream();
        String json = Main.streamToString(in);
        in.close();

        ClustersWithTimeline<Cluster<Location>> clusters = Main.takeoutToClusters(json, 0,
                                                                                  Main.MAX_RADIUS,
                                                                                  Main.MIN_SIZE_RATIO,
                                                                                  Main.ERROR_METERS
        );

        GeoBean geo = (GeoBean) request.getSession().getAttribute("geo");
        geo.setClusters(clusters);
        response.setStatus(200);
        response.getWriter().append(clusters.clusters.size() + " clusters").close();
    }
}
