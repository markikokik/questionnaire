package ru.samis.questionnaire;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import ru.samis.geoclusters.Clustering;
import ru.samis.geoclusters.Main;
import ru.samis.geoclusters.entities.Cluster;
import ru.samis.geoclusters.entities.ClustersWithTimeline;
import ru.samis.geoclusters.entities.Location;
import ru.samis.geoclusters.entities.Place;
import ru.samis.geoclusters.entities.Route;
import ru.samis.questionnaire.db.DataBase;

@ManagedBean(name = "geo")
@SessionScoped
public class GeoBean {
    public GeoBean() {
//        startNewSession();
//        loadClusters();
    }

    private String sessionId;

    public String getSessionId() {
        return sessionId;
    }

    private ClustersWithTimeline<Cluster<Location>> clusters = null;
    private Place[] places = null;
    private List<Route> routes = null;
    private Part file;

    private int radius = Main.MAX_RADIUS;
    private double minSizeRatio = Main.MIN_SIZE_RATIO,
            errorMeters = Main.ERROR_METERS;

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

    public double getMinSizeRatio() {
        return minSizeRatio;
    }

    public void setMinSizeRatio(double minSizeRatio) {
        this.minSizeRatio = minSizeRatio;
    }

    public double getErrorMeters() {
        return errorMeters;
    }

    public void setErrorMeters(double errorMeters) {
        this.errorMeters = errorMeters;
    }

    public ClustersWithTimeline<Cluster<Location>> getClusters() {
        return clusters;
    }

    public void setClusters(ClustersWithTimeline<Cluster<Location>> clusters) {
        this.clusters = clusters;
    }

    public void startNewSession() {
        FacesContext fCtx = FacesContext.getCurrentInstance();
        fCtx.getExternalContext().invalidateSession();
        HttpSession session = (HttpSession) fCtx.getExternalContext().getSession(true);

        sessionId = session.getId();

    }

    public void loadClusters() {
        try {
            FileInputStream in = new FileInputStream("/media/Data/Documents/tmp/clusters.bin");

            ObjectInputStream objectInputStream = new ObjectInputStream(in);
            clusters = (ClustersWithTimeline<Cluster<Location>>) objectInputStream.readObject();

            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private String fileName;

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Part getFile() {
        return file;
    }

    public void setFile(Part file) {
        this.file = file;
        processTakeout();
        this.file = null;
    }

    public void processTakeout() {
        if (file == null) {
            return;
        }

//        new Thread(() -> {
        InputStream in = null;
        try {
            in = file.getInputStream();

            setClusters(null);
            ClustersWithTimeline<Cluster<Location>> clusters = null;
            if (fileName.endsWith("zip")) {
                clusters = Main.takeoutZipToClusters(in, 1496271600,
                                                     radius, minSizeRatio, errorMeters);
            } else if (fileName.endsWith("json")) {
                clusters = Main.takeoutJsonToClusters(in, 1496271600,
                                                      radius, minSizeRatio, errorMeters);
            }
            in.close();
            setClusters(clusters);
        } catch (IOException e) {
            e.printStackTrace();
        }

//        }).start();
    }

    public void doNothing() {

    }

    public Place[] getPlaces() {
        return places;
    }

    public void setPlaces(Place[] places) {
        this.places = places;
    }

    private String routesJson;

    public String getRoutesJson() {
        return routesJson;
    }

    public void setRoutesJson(String routesJson) {
        this.routesJson = routesJson;
        JSONArray array = new JSONArray(routesJson);
        int length = array.length();
        routes = new ArrayList<>(length);
        Route route;

        JSONObject object;
        for (int i = 0; i < length; i++) {
            object = array.getJSONObject(i);

            routes.add(route = new Route());
            route.setSessionId(sessionId);

            route.setGeometry(object.optString("geometry"));
            route.setMoveKinds(object.optString("moveKinds"));
            route.setPeriodic(object.optString("periodic"));
            route.setWeight(object.optInt("weight"));
        }

        try {
            DataBase db = DataBase.getInstance();
            db.insertRoutes(routes);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public String getPointsJson() {
        return "";
    }

    public void setPointsJson(String json) {
        JSONArray array = new JSONArray(json);
        int length = array.length(), id;
        places = new Place[length];
        JSONObject object;

        List<Cluster<Cluster<Location>>> clustersLeft = new ArrayList<>();
        List<Place> placesList = new ArrayList<>();
        Cluster cluster;

        for (int i = 0; i < length; i++) {
            object = array.getJSONObject(i);

            places[i] = new Place();
            places[i].setLat(object.optDouble("x1"));
            places[i].setLng(object.optDouble("y1"));
            places[i].setLat2(object.optDouble("x2"));
            places[i].setLng2(object.optDouble("y2"));

            places[i].name = object.optString("name");
            places[i].typeStr = object.optString("type").toLowerCase();
            places[i].type = Place.TYPE_TO_MASK.get(places[i].typeStr);
            places[i].likeStr = object.optString("likeThisPlace").toLowerCase();
            places[i].like = Place.LIKE_TO_MASK.get(places[i].likeStr);

            places[i].setWeight(object.optInt("weight"));
            places[i].setDailyTimeHrs(object.optInt("dailyTime"));

            places[i].sessionId = sessionId;

            id = object.optInt("id");
            if (id > 0) {
                clustersLeft.add(cluster = clusters.clusters.get(id - 1));
                cluster.id = i;
                places[i].clusterId = i;
            } else {
                placesList.add(places[i]);
            }
//                places[i].timeParams = new RandomTimeParams[1];
//                places[i].timeParams[0] = Clustering.calcClusterOfClustersRndTimeParams(clusters.get(id - 1));
//            } else {

            places[i].timeStr = object.optString("usualTime").toLowerCase();

            if (!"".equals(places[i].timeStr)) {
                String[] times = places[i].timeStr.split(",");
                for (String time : times) {
                    places[i].usualTimeMask |= Place.TIME_TO_MASK.get(time);
                }

//                    RandomTimeParams params = places[i].timeParams = new RandomTimeParams();
            }
//            }
        }

        if (clusters != null) {
            clusters.clusters = clustersLeft;
        }

////        StringBuilder builder = new StringBuilder();
//        try {
////            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
//            FileOutputStream out = new FileOutputStream("/media/Data/Documents/tmp/clusters.bin");
////            ObjectOutputStream objectOutputStream = new ObjectOutputStream();
//            ObjectOutputStream objectOutputStream = new ObjectOutputStream(out);
//            objectOutputStream.writeObject(clusters);
////            byte[] bytes = byteArrayOutputStream.toByteArray();
////            for (byte b : bytes) {
////                builder.append(b).append(", ");
////            }
//            out.close();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }


        routes = Clustering.detectRoutes2(clusters, placesList);

        try {
            DataBase db = DataBase.getInstance();
            db.insertPlaces(places);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public List<Route> getRoutes() {
        return routes;
    }

    public void setRoutes(List<Route> routes) {
        this.routes = routes;
    }

    public void setTestClusters() {
    }
}
