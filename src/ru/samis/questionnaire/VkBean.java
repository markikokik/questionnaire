package ru.samis.questionnaire;

import com.vk.api.sdk.client.TransportClient;
import com.vk.api.sdk.client.VkApiClient;
import com.vk.api.sdk.client.actors.UserActor;
import com.vk.api.sdk.exceptions.ApiException;
import com.vk.api.sdk.exceptions.ClientException;
import com.vk.api.sdk.exceptions.OAuthException;
import com.vk.api.sdk.httpclient.HttpTransportClient;
import com.vk.api.sdk.objects.UserAuthResponse;
import com.vk.api.sdk.objects.users.Occupation;
import com.vk.api.sdk.objects.users.UserXtrCounters;
import com.vk.api.sdk.queries.users.UserField;

import java.sql.SQLException;
import java.util.Calendar;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import ru.samis.questionnaire.db.DataBase;

//@Stateful(name = "ejb")
@ManagedBean(name = "vk")
@SessionScoped
public class VkBean {
    private static final int APP_ID = 6217258;
    private static final String REDIRECT_URI = "http://ssp.geosamara.ru/q/geodata.xhtml",
    //    private static final String REDIRECT_URI = "https://oauth.vk.com/blank.html",
    CLIENT_SECRET = "bkIhgETUU0ejVZ0AS0c8";

    private VkApiClient vk;
    private UserActor actor;

    private String person = "";

    private boolean authorized = false;


    private PersonalData personalData = new PersonalData();

    public PersonalData getPersonalData() {
        return personalData;
    }

    public void setPersonalData(PersonalData personalData) {
        this.personalData = personalData;
    }

    public void setPerson(String json) {
        person = json;
    }

    public String getPerson() {
        return person;
    }


    public boolean isAuthorized() {
        return authorized;
    }

    public int getAppId() {
        return APP_ID;
    }

    public String getRedirectUri() {
        return REDIRECT_URI;
    }

    public VkBean() {

    }


    public boolean authorizeInVK(String code) {

        if (!authorized) {
            if (vk == null) {
                TransportClient transportClient = HttpTransportClient.getInstance();

                vk = new VkApiClient(transportClient);
            }

            UserAuthResponse authResponse = null;
            try {
                authResponse = vk.oauth()
                                 .userAuthorizationCodeFlow(APP_ID, CLIENT_SECRET,
                                                            REDIRECT_URI, code
                                 ).execute();
                actor = new UserActor(authResponse.getUserId(), authResponse.getAccessToken());

                initFromVK();

                return authorized = true;
            } catch (OAuthException e) {
                e.getRedirectUri();
            } catch (ApiException e) {
                e.printStackTrace();
            } catch (ClientException e) {
                e.printStackTrace();
            }
            return authorized = false;
        }
        return authorized;
    }

    private void initFromVK() throws ClientException, ApiException {
        List<UserXtrCounters> counters = vk.users()
                                           .get(actor)
                                           .fields(
                                                   UserField.BDATE,
                                                   UserField.CITY,
                                                   UserField.EDUCATION,
                                                   UserField.SEX,
                                                   UserField.RELATIVES,
                                                   UserField.RELATION,
                                                   UserField.PERSONAL,
                                                   UserField.OCCUPATION,
                                                   UserField.SCHOOLS
                                           )
                                           .execute();
        UserXtrCounters user = counters.get(0);

        personalData.setSex(user.getSex().getValue());
        personalData.setSexFromVK(true);
        personalData.setCity(user.getCity().getTitle());
        personalData.setCityFromVK(true);

        String[] bdateComponents = user.getBdate().split("\\.");
        if (bdateComponents.length == 3) {
            int age;
            Calendar birth = Calendar.getInstance();
            Calendar calendar = Calendar.getInstance();
            birth.set(Integer.parseInt(bdateComponents[2]),
                      Integer.parseInt(bdateComponents[1]) - 1,
                      Integer.parseInt(bdateComponents[0]),
                      0, 0, 0);
            age = calendar.get(Calendar.YEAR) - birth.get(Calendar.YEAR);
            if (calendar.get(Calendar.MONTH) < birth.get(Calendar.MONTH)) {
                age--;
            } else if (calendar.get(Calendar.MONTH) == birth.get(Calendar.MONTH) &&
                       calendar.get(Calendar.DAY_OF_MONTH) < birth.get(Calendar.DAY_OF_MONTH)) {
                age--;
            }
            personalData.setAge(age);
            personalData.setAgeFromVK(true);
        }

        Occupation occupation = user.getOccupation();
        if (occupation != null && "university".equals(occupation.getType())) {
            String educationStatus = user.getEducationStatus();
            if (educationStatus != null && educationStatus.contains("Выпускник")) {
                personalData.setEdu(PersonalData.EDU_HIGH);
                personalData.setEduFromVK(true);
            } else {
                personalData.setEdu(PersonalData.EDU_HIGH_UNFINISHED);
                personalData.setEduFromVK(true);
            }
        }

        Integer relation = user.getRelation();
        if (PersonalData.ALLOWED_FAMILY_STATUSES.contains(relation)) {
            personalData.setFamilyStatus(relation);
            personalData.setFamilyFromVK(true);
        }
    }

    public String savePersonalDataToDb() throws ClassNotFoundException {
        FacesContext fCtx = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) fCtx.getExternalContext().getSession(false);
        if (session == null) {
            session = (HttpSession) fCtx.getExternalContext().getSession(true);
        }
        personalData.setSessionId(session.getId());
        try {
            DataBase db = DataBase.getInstance();
            db.insertPersonalData(personalData);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return "places";
    }
}
