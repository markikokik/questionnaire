package ru.samis.questionnaire;

import java.util.Arrays;
import java.util.List;

//@ManagedBean(name = "person")
//@SessionScoped
public class PersonalData {
    public static final int LIVING_FLAT = 1,
            LIVING_COMMUNAL_FLAT = 2,
            LIVING_RENT_FLAT = 3,
            LIVING_HOSTEL = 4,
            LIVING_HOUSE = 5,
            LIVING_OTHER = 6;

    public static final int OCCUPATION_STUDY = 1,
            OCCUPATION_WORKER = 2,
            OCCUPATION_BUSINESSMAN = 3,
            OCCUPATION_PENS = 4,
            OCCUPATION_HOUSEWIFE = 5,
            OCCUPATION_JOBLESS = 6,
            OCCUPATION_FREELANCER = 7,
            OCCUPATION_MATERNITY = 8,
            OCCUPATION_OTHER = 9;

    public static final byte SEX_MALE = 2,
            SEX_FEMALE = 1;

    public static final byte EDU_SCHOOL = 1,
            EDU_TECH = 2,
            EDU_HIGH_UNFINISHED = 3,
            EDU_HIGH = 4,
            EDU_DEGREE = 5;

    public static final int FAMILY_NOT_MARRIED = 1,
            FAMILY_MARRIED = 4,
            FAMILY_DEVORCED = 30,
            FAMILY_WIDOWS = 40, //вдов
            FAMILY_CLM = 8; //common law marriage

    public static final List<Integer> ALLOWED_FAMILY_STATUSES = Arrays.asList(
            FAMILY_NOT_MARRIED,
            FAMILY_MARRIED,
            FAMILY_CLM
    );

    private byte[] childrenAges;

    private String city,
            activityOther,
            houseOther,sessionId;

    private int age,
            sex,
            familyStatus,
            living,
            occupation,
            appsMask,
            vkId;

    private byte edu;

    private boolean cityFromVK = false,
            sexFromVK = false,
            familyFromVK = false,
            eduFromVK = false,
            ageFromVK = false;

    public boolean isCityFromVK() {
        return cityFromVK;
    }

    public void setCityFromVK(boolean cityFromVK) {
        this.cityFromVK = cityFromVK;
    }

    public boolean isSexFromVK() {
        return sexFromVK;
    }

    public void setSexFromVK(boolean sexFromVK) {
        this.sexFromVK = sexFromVK;
    }

    public boolean isFamilyFromVK() {
        return familyFromVK;
    }

    public void setFamilyFromVK(boolean familyFromVK) {
        this.familyFromVK = familyFromVK;
    }

    public boolean isEduFromVK() {
        return eduFromVK;
    }

    public void setEduFromVK(boolean eduFromVK) {
        this.eduFromVK = eduFromVK;
    }

    public boolean isAgeFromVK() {
        return ageFromVK;
    }

    public void setAgeFromVK(boolean ageFromVK) {
        this.ageFromVK = ageFromVK;
    }

    public int getAppsMask() {
        return appsMask;
    }

    public void setAppsMask(int appsMask) {
        this.appsMask = appsMask;
    }

    public int getOccupation() {
        return occupation;
    }

    public void setOccupation(int occupation) {
        this.occupation = occupation;
    }

    public int getLiving() {
        return living;
    }

    public void setLiving(int living) {
        this.living = living;
    }

    public int getAge() {
        return age;
    }

    public byte[] getChildrenAges() {
        return childrenAges;
    }

    public void setChildrenAges(byte[] childrenAges) {
        this.childrenAges = childrenAges;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getActivityOther() {
        return activityOther == null ? "" : activityOther;
    }

    public void setActivityOther(String activityOther) {
        this.activityOther = activityOther;
    }

    public String getHouseOther() {
        return houseOther == null ? "" : houseOther;
    }

    public void setHouseOther(String houseOther) {
        this.houseOther = houseOther;
    }

    public int getSex() {
        return sex;
    }

    public void setSex(int sex) {
        this.sex = sex;
    }

    public byte getEdu() {
        return edu;
    }

    public void setEdu(byte edu) {
        this.edu = edu;
    }

    public int getFamilyStatus() {
        return familyStatus;
    }

    public void setFamilyStatus(int familyStatus) {
        this.familyStatus = familyStatus;
    }

    public String getChildrenCount() {
        return childrenAges != null ? String.valueOf(childrenAges.length) : "";
    }

    public void setChildrenCount(String childrenCount) {

    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }
}
