package ru.samis.questionnaire.db;

import java.util.HashMap;
import java.util.Map;

public abstract class DbConst {
    static abstract class RouteEntry {
        public static final String TABLE_NAME = "routes";
        public static final Map<String, String> COLUMNS = new HashMap<>();

        static {
            COLUMNS.put("geometry", "varchar");
            COLUMNS.put("moveKinds", "varchar");
            COLUMNS.put("periodic", "varchar");
            COLUMNS.put("sessionId", "varchar");
            COLUMNS.put("weight", "int");
        }
    }

    static abstract class PlacesEntry {
        public static final String TABLE_NAME = "places";
        public static final Map<String, String> COLUMNS = new HashMap<>();

        static {
            COLUMNS.put("likeStr", "varchar");
            COLUMNS.put("name", "varchar");
            COLUMNS.put("sessionId", "varchar");
            COLUMNS.put("timeStr", "varchar");
            COLUMNS.put("typeStr", "varchar");
            COLUMNS.put("lat", "float");
            COLUMNS.put("lat2", "float");
            COLUMNS.put("lng", "float");
            COLUMNS.put("lng2", "float");
            COLUMNS.put("weight", "int");
        }
    }

    static abstract class UsersEntry {
        public static final String TABLE_NAME = "users";
        public static final Map<String, String> COLUMNS = new HashMap<>();

        static {
            COLUMNS.put("activityOther", "varchar");
            COLUMNS.put("age", "TinyInt");
            COLUMNS.put("appsMask", "int");
            COLUMNS.put("city", "varchar");
            COLUMNS.put("familyStatus", "TinyInt");
            COLUMNS.put("houseOther", "varchar");
            COLUMNS.put("living", "TinyInt");
            COLUMNS.put("occupation", "TinyInt");
            COLUMNS.put("sex", "TinyInt");
            COLUMNS.put("sessionId", "varchar");
        }
    }

    public static final String INSERT_PERSONAL_DATA,
            INSERT_ROUTE,
            INSERT_PLACE;

    static {
        INSERT_PERSONAL_DATA = buildInsert(UsersEntry.TABLE_NAME, UsersEntry.COLUMNS);
        INSERT_PLACE = buildInsert(PlacesEntry.TABLE_NAME, PlacesEntry.COLUMNS);
        INSERT_ROUTE = buildInsert(RouteEntry.TABLE_NAME, RouteEntry.COLUMNS);
    }

    private static String buildInsert(String tableName, Map<String, String> columns) {
        StringBuilder builder = new StringBuilder("insert into ").append(tableName).append(" (");
        for (String column : columns.keySet()) {
            builder.append(column).append(", ");
        }
        builder.deleteCharAt(builder.length() - 1);
        builder.deleteCharAt(builder.length() - 1);
        builder.append(") values (?");
        for (int i = 1; i < columns.size(); i++) {
            builder.append(", ?");
        }
        builder.append(")");
        return builder.toString();
    }
}
