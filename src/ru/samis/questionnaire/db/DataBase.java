package ru.samis.questionnaire.db;

import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import ru.samis.geoclusters.entities.Place;
import ru.samis.geoclusters.entities.Route;
import ru.samis.questionnaire.PersonalData;

public class DataBase extends DbConst {
    private static DataBase instance;

    private Connection conn;

    private static final String
            USERNAME = "rgis",
            PASSWORD = "rgis",
            SERVER = "192.168.12.10",
            DB_NAME = "socialEnv",
            URL = "jdbc:sqlserver://" + SERVER + ";databaseName=" + DB_NAME;

    public void insertPlace(Place data) throws SQLException {
        insert(INSERT_PLACE, PlacesEntry.COLUMNS, data);
    }

    public void insertRoutes(Iterable<Route> data) throws SQLException {
        insert(INSERT_ROUTE, RouteEntry.COLUMNS, data);
    }

    public void insertPlaces(Place[] data) throws SQLException {
        insert(INSERT_PLACE, PlacesEntry.COLUMNS, data);
    }

    public void insertPersonalData(PersonalData data) throws SQLException {
        insert(INSERT_PERSONAL_DATA, UsersEntry.COLUMNS, data);
    }

    public void insert(String insertSql, Map<String, String> columns, Object data) throws SQLException {
        PreparedStatement statement = conn.prepareStatement(insertSql);

        Class dataClass = data.getClass();
        Field field;
        int i = 1;
        try {
            for (String columnName : columns.keySet()) {
                field = dataClass.getDeclaredField(columnName);
                field.setAccessible(true);

                statement.setObject(i++, field.get(data));
            }
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        statement.execute();
        conn.commit();
    }

    private Field findField(Class cls, String name) throws NoSuchFieldException {
        while (cls != null) {
            for (Field field : cls.getDeclaredFields()) {
                if (field.getName().equals(name)) {
                    return field;
                }
            }
            cls = cls.getSuperclass();
        }
        return null;
    }

    public void insert(String insertSql, Map<String, String> columns, Iterable<? extends Object> data) throws SQLException {
        PreparedStatement statement = conn.prepareStatement(insertSql);

        try {
            Class dataClass = data.iterator().next().getClass();
            List<Field> fields = new ArrayList<>(columns.size());
            Field field;
            for (String columnName : columns.keySet()) {
                field = findField(dataClass, columnName);
                field.setAccessible(true);
                fields.add(field);
            }

            for (Object obj : data) {
                int i = 1;
                for (Field field1 : fields) {
                    statement.setObject(i++, field1.get(obj));
                }

                statement.addBatch();
            }


        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        statement.executeBatch();
        conn.commit();
    }

    public static DataBase getInstance() throws ClassNotFoundException, SQLException {
        if (instance == null) {
            instance = new DataBase();
        }
        return instance;
    }

    public DataBase() throws ClassNotFoundException, SQLException {
        Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

        conn = DriverManager.getConnection(URL, USERNAME, PASSWORD);
    }
}
