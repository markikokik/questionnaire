package ru.samis.questionnaire.conversion;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

@FacesValidator("ageValidator")
public class AgeValidator implements Validator {
    @Override
    public void validate(FacesContext facesContext, UIComponent uiComponent, Object o) throws ValidatorException {
        String s;
        if (o == null) {
            s = "Возраст должен быть числом!";
        } else {
            Integer age = (Integer) o;
            if (age >= 0) {
                return;
            } else {
                s = "Возраст не может быть отрицательным";
            }
        }

        FacesMessage msg = new FacesMessage("Неверно введён возраст", s);
        msg.setSeverity(FacesMessage.SEVERITY_ERROR);
        throw new ValidatorException(msg);
    }
}
