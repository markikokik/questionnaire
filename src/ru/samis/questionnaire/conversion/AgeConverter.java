package ru.samis.questionnaire.conversion;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

@FacesConverter("AgeConverter")
public class AgeConverter implements Converter {
    @Override
    public Integer getAsObject(FacesContext facesContext, UIComponent uiComponent, String s) {
        if ("".equals(s.trim())) {
            return 0;
        }
        try {
            return Integer.parseInt(s);
        } catch (NumberFormatException e) {
            return null;
        }
    }

    @Override
    public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object o) {
        if (((Integer) o) == 0) {
            return "";
        }
        return o.toString();
    }
}
