package ru.samis.questionnaire.conversion;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

@FacesValidator("childrenAgesValidator")
public class ChildrenAgesValidator implements Validator {
    @Override
    public void validate(FacesContext context, UIComponent component, Object o) throws ValidatorException {
        String msgText = null;
        if (o == null) {
            msgText = "Возраст должен быть числом";
        } else {
            byte[] ages = (byte[]) o;
            for (byte age : ages) {
                if (age < 0) {
                    msgText = "Возраст не может быть отрицательным";
                    break;
                }
            }
        }

        if (msgText == null) {
            return;
        }

        FacesMessage msg = new FacesMessage("Неверно введён возраст", msgText);
        msg.setSeverity(FacesMessage.SEVERITY_ERROR);
        throw new ValidatorException(msg);
    }
}
