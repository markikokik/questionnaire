package ru.samis.questionnaire.conversion;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.validator.ValidatorException;

@FacesConverter("ChildrenAgesConverter")
public class ChildrenAgesConverter implements Converter {
    @Override
    public byte[] getAsObject(FacesContext facesContext, UIComponent uiComponent, String s) {
        return parseChildrenAges(s);
    }

    @Override
    public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object o) {
        byte[] childrenAges = (byte[]) o;
        if (childrenAges.length > 0) {
            StringBuilder builder = new StringBuilder();
            builder.append(childrenAges[0]);
            for (int i = 1; i < childrenAges.length; i++) {
                builder.append(", ").append(childrenAges[i]);
            }
            return builder.toString();
        }

        return "";
    }

    public static byte[] parseChildrenAges(String s) throws ValidatorException {
        if ("".equals(s)) {
            return new byte[0];
        }
        while (s.length() > (s = s.replaceAll(" ", "")).length()) {

        }
        String[] ages = s.split(",");
        byte[] childrenAges = new byte[ages.length];
        try {
            for (int i = 0; i < ages.length; i++) {
                childrenAges[i] = Byte.parseByte(ages[i]);
            }
            return childrenAges;
        } catch (NumberFormatException e) {
            return null;
        }
    }
}
