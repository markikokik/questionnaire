function vk_popup(options) {
    var
        screenX = typeof window.screenX != 'undefined' ? window.screenX : window.screenLeft,
        screenY = typeof window.screenY != 'undefined' ? window.screenY : window.screenTop,
        outerWidth = typeof window.outerWidth != 'undefined' ? window.outerWidth : document.body.clientWidth,
        outerHeight = typeof window.outerHeight != 'undefined' ? window.outerHeight : (document.body.clientHeight - 22),
        width = options.width,
        height = options.height,
        left = parseInt(screenX + ((outerWidth - width) / 2), 10),
        top = parseInt(screenY + ((outerHeight - height) / 2.5), 10),
        features = (
            'width=' + width +
            ',height=' + height +
            ',left=' + left +
            ',top=' + top
        );
    return window.open(options.url, 'vk_oauth', features);
}

function doLogin() {
    var win;
    var uri_regex = new RegExp(redirect_uri);
    win = vk_popup({
        width: 620,
        height: 370,
        url: url
    });

    var watch_timer = setInterval(function () {
        try {
            if (uri_regex.test(win.location)) {
                clearInterval(watch_timer);

                setTimeout(function () {
                    win.close();
                    document.location.reload();
                }, 500);
            }
        } catch (e) {

        }
    }, 100);
}

function getVKcode() {

    // vkCallback = function (session, status) {
    //     alert(session);
    // }
    //
    // var script = document.createElement('SCRIPT');
    // script.src = "https://vk.com/js/api/openapi.js?150";
    // script.onload = function () {
    //     VK.init({
    //         apiId: client_id
    //     });
    //
    //     VK.Auth.login(vkCallback, 65536);
    // };
    // var head = document.getElementsByTagName("head")[0];
    // head.appendChild(script);

    // doLogin();
    window.location.href = url;
}


var takeoutUrl = 'https://takeout.google.com/settings/takeout?hl=ru&amp;gl&amp;expflags';

function clearFileSelect() {
    document.getElementById('takeoutForm:cmdSelectFile')
        .value = "";
}

function onFileSelect() {
    var cmdSelectFile = document.getElementById('takeoutForm:cmdSelectFile');

    var request = new XMLHttpRequest();

    /*request.onreadystatechange = function () { // (3)
        if (request.readyState != 4) return;

        if (request.status != 200) {
            alert(request.status + ': ' + request.statusText);
        } else {
            alert(request.responseText);
        }
    }*/

    request.open("POST", "/q/androUpload");
    request.send(cmdSelectFile.files[0]);
}