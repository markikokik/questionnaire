$(function() {

	var carousel = $('.jcarousel').jcarousel({
 		
	});

	$('.jcarousel-prev').jcarouselControl({
		target: '-=1'
	});

	$('.jcarousel-next').jcarouselControl({
		target: '+=1'
	});

	$('.jcarousel-pagination').jcarouselPagination({
		carousel: carousel
	});

	$('.jcarousel-pagination')
		.on('active.jcarouselpagination', 'a', function() {
			$(this).addClass('active');
		})
		.on('inactive.jcarouselpagination', 'a', function() {
			$(this).removeClass('active');
		}).jcarouselPagination();


	/* Selectbox
	----------------------------------------------------------------------------------------------------*/
	$('.f-select').on('click', '.f-select__current', function() {

		$('.f-select').css('z-index', '5');
		$(this).closest('.f-select').css('z-index', '10');

		var selectItems = $(this).next('.f-select__items');
		(selectItems.css('display') == 'none') ? selectItems.css('display', 'block') : selectItems.css('display', 'none');

	});

	$('.f-select').on('click', '.f-select__item', function() {

		var select = $(this).closest('.f-select');

		select.find('.f-select__current').find('span').text( $(this).find('.f-select__value').text() );
		select.find('.f-select__items').css('display', 'none');

	});

	$('.f-select').on('mouseleave', function() {
		
		$(this).find('.f-select__items').css('display', 'none');

	});


	/* Radio buttons
	----------------------------------------------------------------------------------------------------*/
	$('.f-radio').on('change', 'input', function() {

		var radio = $(this).closest('.f-radio');

		if ( radio.hasClass('state-active') ) {
			radio.removeClass('state-active');
            $(radio).find('input').attr('checked', false);
        }
		else {
			$(this).closest('.f-radio-group').find('.f-radio').removeClass('state-active');
			radio.addClass('state-active');
            $(radio).find('input').attr('checked', true);
        }

	});


	/* Show layer link
	----------------------------------------------------------------------------------------------------*/
	$('.js-show-layer').on('click', function() {

		var layer = $('#' + $(this).data('layer'));
		
		if (layer.length) {

			if (layer.css('display') === 'none') {

				$(this).addClass('state-active').find('.link').text('Скрыть список всех слоев');

				layer.css({
					'display' : 'block'
				}).animate({
					'opacity' : '1'
				});

			}
			else {

				$(this).removeClass('state-active').find('.link').text('Показать список всех слоев');

				layer.css({
					'display' : 'none',
					'opacity' : '0'
				});

			}

		}

	});

	/* Comments
	----------------------------------------------------------------------------------------------------*/
	$('#NewsCommentsTitle').on('click', 'span', function() {

		var commentsIcon = $('#NewsCommentsTitle').find('.icon');
		var comments = $('#NewsCommentsList');

		if(comments.css('display') === 'block') {
			comments.slideUp(250, function(){
				commentsIcon.addClass('state-collapsed');	
			});
		}
		else {
			commentsIcon.removeClass('state-collapsed');
			comments.slideDown(250);
		}

	});


	$('.js-full-text').on('click', 'span', function() {

		var _parent = $(this).closest('.js-full-text');
		var _link = _parent.find('.link');
		var _icon = _parent.find('.icon');
		var _content = _parent.prev('.js-collapsed');

		if (_content.css('display') === 'block') {
			_content.slideUp();
			_link.text('Полный текст');	
			_icon.removeClass('collapse');
		}
		else {
			_content.slideDown();	
			_link.text('Краткий текст');
			_icon.addClass('collapse');
		}
		

	});

	/* TODO: Just for preview
	----------------------------------------------------------------------------------------------------*/
	$('#NewsComments').on('click', '.g-icon-delete', function() {

		$(this).closest('.b-comment').addClass('b-comment_deleted').text('Комментарий удален');
		return false;

	})


	/* Home Projects aka Carousel
	----------------------------------------------------------------------------------------------------*/
	$('#HomeProjects').on('click', '.js-home-carousel-item', function() {
		
		var _this = $(this);
		var _details = _this.find('.content');

		if (!_this.hasClass('state-active')) {

			_this.addClass('state-active');
			_this.closest('.jcarousel-inner').height(500);

			ui.showLayer( _details, true , function() {

				$('body').on('click', function(e) {
					if (($(e.target).closest( '.js-home-carousel-item' ).length === 0) || ($(e.target).attr('class') === 'close')) {
						_this.removeClass('state-active');
						ui.hideLayer( _details, true, function() {
							_this.closest('.jcarousel-inner').height(200);
						});
					}
				});


			});

		}

	});


	/* User layers checkboxes events
	----------------------------------------------------------------------------------------------------*/
	$('#UserLayers').on('click', '.js-user-layer-link', function() {

		var _layer = $(this).closest('.b-user-layer');
		var _checkbox = _layer.find('.js-checkbox');

		if (_layer.hasClass('state-active')) {
			_layer.removeClass('state-active');
			_checkbox.parent().removeClass('checkbox-checked');
			_checkbox.prop('checked', false);
		}
		else {
			_layer.addClass('state-active');
			_checkbox.parent().addClass('checkbox-checked');
			_checkbox.prop('checked', true);
		}

	});

	$('#btnCreateLayer').on('click', function() {

		ui.showPopup( $('#templateCreateNewLayer'), function() {});

	});

	$('#btnCreateStyle').on('click', function() {

		ui.showPopup( $('#templatebtnCreateStyle'), function() {});

	});


	$('#SelectStyleType').on('change', function() {

		var style = $(this).val();

		$('.js-style-1').css('display', 'none');
		$('.js-style-2').css('display', 'none');
		$('.js-style-3').css('display', 'none');

		$('.js-style-' + style).css('display', 'block');

	});


	if ($('.color-box').length){

			$('.color-box').colpick({
				colorScheme:'light',
				layout:'rgbhex',
				color:'ff8800',
				onSubmit:function(hsb,hex,rgb,el) {
					$(el).css('background-color', '#'+hex);
					$(el).colpickHide();
					$(el).parent().find('.color').val(hex);
				}
			}).css('background-color', '#ff8800');

	}

	// Tabs init
	$('.js-tabs').on('click', '.js-tab', function() {
		
		var _tabs = $(this).closest('.js-tabs');
		var _tab = $(this).data('tab');
		var _content = $('.js-item-' + _tab);

		_tabs.find('.js-tab').removeClass('state-active');
		_tabs.find('.js-item').removeClass('state-active');

		$(this).addClass('state-active');
		_content.addClass('state-active');

	});


	$('.js-collapsible-link').on('click', '.link', function() {

		var _item = $(this).closest('.js-collapsible-link');
		var _section = _item.next('.js-collapsible-content');
		
		if(_section.css('display') === 'block') {
			_section.slideUp(150);
			_item.removeClass('state-active');
		}
		else {
			_section.slideDown(150);
			_item.addClass('state-active');
		}

	});

	$('.b-object').on('click', '.icon-map', function() {

		var _object = $(this).closest('.b-object');
		var _mapLayer = _object.find('.b-object__map');
		if (_mapLayer.length) {

			if (_object.hasClass('state-with-map')) {
				_object.removeClass('state-with-map')
			}
			else {
				_object.addClass('state-with-map');
			}

		}

	});

	// Custom selects (are based on Cusel)
	/*var cuselParams = {
		changedEl: "select.cusel",
		visRows: 5
	}
    cuSel( cuselParams );*/


});
