var person = new Object();

var controls = ["sex",
    "age",
    "occupation",
    "occupation_another",
    "education",
    "family_status",
    "childs",
    "childs_age",
    "living"];

onControlChange = function () {
    person[this.name] = this.value;
}

onAppsChange = function () {
    person.apps ^= this.value;
}

propToRadioState = function (radioName, fieldName, formName) {
    if (formName) {
        // radioName = formName + ':' + radioName;
        fieldName = formName + ':' + fieldName;
    }

    var radios = document.getElementsByName(radioName);
    var field = document.getElementById(fieldName);
    for (var j = 0; j < radios.length; j++) {
        if (radios[j].value == field.value) {
            radios[j].checked = true;
            var radio = $(radios[j]).closest('.f-radio');
            $(this).closest('.f-radio-group').find('.f-radio').removeClass('state-active');
            radio.addClass('state-active');
        }
    }
}

radioStateToProp = function (radioName, fieldName, formName) {
    if (formName) {
        // radioName = formName + ':' + radioName;
        fieldName = formName + ':' + fieldName;
    }
    var radios = document.getElementsByName(radioName);
    var field = document.getElementById(fieldName);
    for (var j = 0; j < radios.length; j++) {
        if (radios[j].checked) {
            field.value = radios[j].value;
        }
    }
}

appsCheckboxesStateToProp = function (fieldName, formName) {
    if (formName) {
        fieldName = formName + ':' + fieldName;
    }
    var chks = document.getElementsByName('apps');
    var field = document.getElementById(fieldName);
    var mask = 0;
    for (var j = 0; j < chks.length; j++) {
        if (chks[j].checked) {
            mask += +chks[j].value;
        }
    }
    field.value = mask;
}

propToChksState = function (fieldName, formName) {
    if (formName) {
        fieldName = formName + ':' + fieldName;
    }
    var chks = document.getElementsByName('apps');
    var mask = document.getElementById(fieldName).value;
    for (var j = 0; j < chks.length; j++) {
        chks[j].checked = (chks[j].value & mask) > 0;
    }
}

var mainFormName = 'mainForm';

var fieldPairs = [
    ['sex', 'sexHdn'],
    ['occupation', 'occupationHdn'],
    ['education', 'educationHdn'],
    ['family_status', 'family_statusHdn'],
    ['living', 'livingHdn'],
];

propsToBean = function () {
    for (var i = 0; i < fieldPairs.length; i++)
        radioStateToProp(fieldPairs[i][0], fieldPairs[i][1], mainFormName);
}

for (var i = 0; i < fieldPairs.length; i++)
    propToRadioState(fieldPairs[i][0], fieldPairs[i][1], mainFormName);

propToChksState('appsHdn', mainFormName);
// for (var i = 0; i < controls.length; i++) {
//     var radios = document.getElementsByName(controls[i]);
//     for (var j = 0; j < radios.length; j++) {
//         radios[j].onchange = onControlChange;
//     }
// }
//
// var apps = document.getElementsByName("apps");
// for (var j = 0; j < apps.length; j++) {
//     apps[j].onchange = onAppsChange;
// }