
var ui = ui || {}

ui = {

	// Show layer
	showLayer : function( layer, animation, callback ) {

		if (animation) {
			layer
				.css('display', 'block')
				.stop()
				.animate(
					{'opacity' : '1'}, 150, function() {
						callback();
					});
		}
		else {
			layer
				.css({
					'opacity' : '1',
					'display' : 'block'
				});
			callback();
		}

	},

	// Hide layer
	hideLayer : function( layer, animation, callback ) {

		if (animation) {
			layer.stop().animate({
				'opacity' : '0'
			}, 150, function() {
				layer.css('display', 'none');
				callback();
			})
		}
		else {
			layer
				.css({
					'opacity' : '0',
					'display' : 'none'
				});
			callback();
		}

	},

	// Get window sizes
	getWindowSizes : function() {

		return {
			width : $(window).width(),
			height : $(window).height()
		}

	},

	// Render table html from data object
	renderTable : function( data, tableTemplate ) {

		var _compile = _.template( tableTemplate );
		return _compile( { data : data } );

	},

	_pageOverlay : function() {

		

		var _pageSizes = this.getWindowSizes();
		var _cssOverlay = 'b-overlay';

		$('.' + _cssOverlay).remove();

		var _overlay = $('<div>')
			.addClass( _cssOverlay )
			.css({
				width: '10000px',
				height: '10000px'
			});

		$('body').append( _overlay );

	},

	showPopup : function( template, callback ) {

		this._pageOverlay();
		$('.b-popup').css('display', 'none');
		
		template.css('display', 'block');
		
		// callback as string
		callback();

		$('.b-overlay, .js-close').on('click', function() {
			$('.b-popup').css('display', 'none');
			$('.b-overlay').remove();
		})

	}

}



$(document).ready(function(){

	$(".ui-custom-checkbox").mousedown(function() {
		changeCheck($(this));
	}); // down
	
	$(".ui-custom-checkbox").each(function() {
		changeCheckStart($(this));
	}); // each

}); // document ready

function changeCheck(el) {
	var el = el,
		input = el.find("input").eq(0);
	if(!input.attr("checked")) {
		el.css("background-position","0 -14px");	
		input.attr("checked", true)
	}
	else {
		el.css("background-position","0 0");	
		input.attr("checked", false)
	} // if
	var sList = "";
	$("input[name='layersIdx']:checked:enabled").each(function () {
		var cbText = $(this).parent().parent().find("label").html();
	    sList += (sList=="" ? cbText : ", " + cbText);
	});
	$("#layers").val(sList);
	return true;
} // function

function changeCheckStart(el) {
	var el = el,
		input = el.find("input").eq(0);
	if(input.attr("checked")) {
		el.css("background-position","0 -14px");	
	} // if
	return true;
} // function

$(document).ready(function(){

$(".ui-custom-radio").each(function() {
     changeRadioStart($(this));
});


});


function changeRadio(el) {
	var el = el,
		input = el.find("input").eq(0);
	var nm=input.attr("name");
		
	$(".ui-custom-radio input").each(function() {
		if($(this).attr("name")==nm)
		{
			$(this).parent().removeClass("ui-custom-radio-checked");
		}
	});					  
	
	if(el.attr("class").indexOf("ui-custom-radio-disabled")==-1)
	{	
		el.addClass("ui-custom-radio-checked");
		input.attr("checked", true);
	}
	
    return true;
}

function changeVisualRadio(input){
	var wrapInput = input.parent();
	var nm=input.attr("name");
		
	$(".ui-custom-radio input").each(
	
	function() {
     
		if($(this).attr("name")==nm)
		{
			$(this).parent().removeClass("ui-custom-radio-checked");
		}
	   
	   
	});

	if(input.attr("checked")) 
	{
		wrapInput.addClass("ui-custom-radio-checked");
	}
}

function changeRadioStart(el) {

try
{
var el = el,
	radioName = el.attr("name"),
	radioId = el.attr("id"),
	radioChecked = el.attr("checked"),
	radioDisabled = el.attr("disabled"),
	radioTab = el.attr("tabindex"),
	radioValue = el.attr("value");
	if(radioChecked)
		el.after("<span class='ui-custom-radio ui-custom-radio-checked'>"+
			"<input type='radio'"+
			"name='"+radioName+"'"+
			"id='"+radioId+"'"+
			"checked='"+radioChecked+"'"+
			"tabindex='"+radioTab+"'"+
			"value='"+radioValue+"' /></span>");
	else
		el.after("<span class='ui-custom-radio'>"+
			"<input type='radio'"+
			"name='"+radioName+"'"+
			"id='"+radioId+"'"+
			"tabindex='"+radioTab+"'"+
			"value='"+radioValue+"' /></span>");
	
	if(radioDisabled)
	{
		el.next().addClass("ui-custom-radio-disabled");
		el.next().find("input").eq(0).attr("disabled","disabled");
	}
	else {
		el.next().bind("mousedown", function(e) { changeRadio($(this)) });
	}
	el.next().find("input").css("display", "none");
	if($.browser.msie)	el.next().find("input").eq(0).bind("click", function(e) { changeVisualRadio($(this)) });	
	else el.next().find("input").eq(0).bind("change", function(e) { changeVisualRadio($(this)) });
	el.remove();
}
catch(e)
{
}

    return true;
}



