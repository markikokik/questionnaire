var map = new GeosamaraMap();
map.domain = "http://map.samadm.ru";
map.page = "/samara/";
map.modules = "Map,Toolbar,Messenger";
map.instruments = "ScaleInstrument,MoveInstrument,JSIntegration";
map.wms = "/wms1";
map.wfs = "/wfs1";
map.settings = "/settings1";
map.layers = "SM1,SM2,SM3,SM4";
map.scalesList = "200000,100000,50000,20000,10000,5000,2000,1000";
map.scale = 3;
map.x = 420;
map.y = 420;
map.selected = "";
map.insert("mapContainer");

var POINTS_LAYER_ID = "POINTS_LAYER_ID";
var POINTS_STYLE_ID = "distanceCheckpoint";
var ROUTE_STYLE_ID = "UserRoute";

var points = [];

var currentRouteIndex = -1;
var fromPointIndex = -1;
var toPointIndex = -1;


function getMovie() {
    if (typeof  window.gpClient != 'undefined') {
        return window.gpClient.getMovieAPI();
    } else {
        var M$ = navigator.appName.indexOf("Microsoft") != -1;
        return (M$ ? window : document)["GeoportalMap"];
    }
}

function imReady() {
    getMovie().createLocalLayer(POINTS_LAYER_ID, 'места, ЦПН');

    if (routes.length > 0) {
        showForRoute(0);
    }
}

function createPointsMarkers() {
    for (var i = 0; i < points.length; i++) {
        getMovie().addListener("MARKER_CREATION", "createPointsMarkersCompleteHandler");

        var geometry = [{"x": (points[i].x1 + points[i].x2) / 2, "y": (points[i].y1 + points[i].y2) / 2}];

        geometry = JSON.parse(getMovie().transformCoordinates(JSON.stringify(geometry), "WGS84", "Samara"));
        for (var j = 0; j < geometry.length; j++) {
            var t = geometry[j].x;
            geometry[j].x = geometry[j].y;
            geometry[j].y = t;
        }

        getMovie().createMarker(POINTS_LAYER_ID, POINTS_STYLE_ID, JSON.stringify(geometry), points[i].name, i);
    }
}

function createPointsMarkersCompleteHandler(responce) {
    getMovie().removeListener("MARKER_CREATION", "createPointsMarkersCompleteHandler");
    var createdMarkersResult = JSON.parse(responce);
    var markerInfo = JSON.parse(getMovie().getMarkerInfo(createdMarkersResult.markerId));
    points[markerInfo.description].markerId = createdMarkersResult.markerId;

    console.log("created " + markerInfo.description + " " + points[markerInfo.description].markerId);
}


function createPointMarker(pointIndex, routeIndex) {
    var geometry = [{
        "x": (points[pointIndex].x1 + points[pointIndex].x2) / 2,
        "y": (points[pointIndex].y1 + points[pointIndex].y2) / 2
    }];
    geometry = JSON.parse(getMovie().transformCoordinates(JSON.stringify(geometry), "WGS84", "Samara"));
    for (var j = 0; j < geometry.length; j++) {
        var t = geometry[j].x;
        geometry[j].x = geometry[j].y;
        geometry[j].y = t;
    }
    getMovie().createMarker(POINTS_LAYER_ID, POINTS_STYLE_ID, JSON.stringify(geometry), points[pointIndex].name, routeIndex);
}

function createRouteMarker(index) {
    var geometry;
    if (routes[index].geometry.length > 0) {
        geometry = JSON.parse(routes[index].geometry);
    }
    else {
        geometry = [{
            "x": (points[routes[index].pointFromIndex].x1 + points[routes[index].pointFromIndex].x2) / 2,
            "y": (points[routes[index].pointFromIndex].y1 + points[routes[index].pointFromIndex].y2) / 2
        }, {
            "x": (points[routes[index].pointToIndex].x1 + points[routes[index].pointToIndex].x2) / 2,
            "y": (points[routes[index].pointToIndex].y1 + points[routes[index].pointToIndex].y2) / 2
        }];
    }

    geometry = JSON.parse(getMovie().transformCoordinates(JSON.stringify(geometry), "WGS84", "Samara"));
    for (var j = 0; j < geometry.length; j++) {
        var t = geometry[j].x;
        geometry[j].x = geometry[j].y;
        geometry[j].y = t;
    }
    getMovie().createMarker(POINTS_LAYER_ID, ROUTE_STYLE_ID, JSON.stringify(geometry), points[routes[index].pointFromIndex].name + " - " + points[routes[index].pointToIndex].name, index);
}

function createRouteMarkersCompleteHandler(responce) {
    var createdMarkersResult = JSON.parse(responce);
    var markerInfo = JSON.parse(getMovie().getMarkerInfo(createdMarkersResult.markerId));

    if (markerInfo.style == ROUTE_STYLE_ID) {
        getMovie().removeListener("MARKER_CREATION", "createRouteMarkersCompleteHandler");
        routes[markerInfo.description].markerRouteID = createdMarkersResult.markerId;
        getMovie().moveToMarker(createdMarkersResult.markerId, true);

        getMovie().startEditMarker(createdMarkersResult.markerId);
    }
}

function updateRoutesList() {
    var routesListHtml = "";
    for (var i = 0; i < routes.length; i++) {
        routesListHtml += "<p><a href='#' onclick='showForRoute(" + i + "); return false;'>" + points[routes[i].pointFromIndex].name + " &mdash; " + points[routes[i].pointToIndex].name + "</a></p>";
    }
    $('#routesList').html(routesListHtml);
}

function saveCurrentRouteGeometry() {
    if (currentRouteIndex >= 0) {
        var geometry = JSON.parse(getMovie().getMarkerGeometry(routes[currentRouteIndex].markerRouteID));
        for (var j = 0; j < geometry.length; j++) {
            var t = geometry[j].x;
            geometry[j].x = geometry[j].y;
            geometry[j].y = t;
        }
        var geometryJSON = getMovie().transformCoordinates(JSON.stringify(geometry), "Samara", "WGS84");
        routes[currentRouteIndex].geometry = geometryJSON;
    }
}

function showForRoute(index) {
    stopAllAddRouteActions();

    saveCurrentRouteGeometry();
    currentRouteIndex = index;

    getMovie().clearLocalLayer(POINTS_LAYER_ID);

    routes[index].markerRouteID = "";

    getMovie().addListener("MARKER_CREATION", "createRouteMarkersCompleteHandler");

    createPointMarker(routes[index].pointFromIndex, index);
    createPointMarker(routes[index].pointToIndex, index);
    createRouteMarker(index);


    var moveKindsItems = [];
    moveKindsItems.push({"name": "пешком", "value": "foot"});
    moveKindsItems.push({"name": "автомобиль", "value": "car"});
    moveKindsItems.push({"name": "такси", "value": "taxi"});
    moveKindsItems.push({"name": "общественный транспорт", "value": "public_transport"});


    var routesOptionsHtml = "";

    routesOptionsHtml += '<div class="b-form b-properties_form">';
    routesOptionsHtml += '	<div class="b-form__row">';
    routesOptionsHtml += '		<div class="b-form__left_col">Способ перемещения</div>';
    routesOptionsHtml += '		<div class="b-form__right_col">';
    for (var i = 0; i < moveKindsItems.length; i++) {
        var stateOfCheck = ""
        if (routes[index].hasOwnProperty("moveKinds")) {
            var variants = routes[index].moveKinds.split(",");
            if (variants.indexOf(moveKindsItems[i].value) >= 0) {
                stateOfCheck = 'checked="checked"';
            }
        }
        routesOptionsHtml += '			<div class="checkbox"><span class="ui-custom-checkbox">';
        routesOptionsHtml += '				<input type="checkbox" ' + stateOfCheck + ' " name="moveKinds_' + i + '" id="moveKinds_' + i + '" onclick="moveKindsCheckHandler(' + index + ', \'' + moveKindsItems[i].value + '\');" /></span>';
        routesOptionsHtml += '				<label for="moveKinds_' + i + '">' + moveKindsItems[i].name + '</label></div>';
    }
    routesOptionsHtml += '		</div></div>';


    var movingTime = ( routes[index].hasOwnProperty("movingTime") ? routes[index].movingTime : '');

    routesOptionsHtml += '<div class="b-form__row">';
    routesOptionsHtml += '<div class="b-form__left_col">Обычно занимает</div>';
    routesOptionsHtml += '<div class="b-form__right_col">';
    routesOptionsHtml += '<div class="b-form__field">';
    routesOptionsHtml += '<input type="text" name="movingTime" id="movingTime" class="f-input-text" value="' + movingTime + '" placeholder="20" onchange="movingTimeChangeHandler(' + index + ')"  style="width: 100px;" />&nbsp;минут';
    routesOptionsHtml += '</div></div></div>';

    routesOptionsHtml += '<div class="g-clear"></div>';

    var periodicItems = [];
    periodicItems.push({"name": "каждый день", "value": "everyday"});
    periodicItems.push({"name": "в будни", "value": "weekdays"});
    periodicItems.push({"name": "в выходные", "value": "weekends"});
    periodicItems.push({"name": "нерегулярно", "value": "unregular"});

    routesOptionsHtml += '<div class="b-form__row">';
    routesOptionsHtml += '<div class="b-form__left_col">Обычно совершаю его</div>';
    routesOptionsHtml += '<div class="b-form__right_col">';
    routesOptionsHtml += '<div class="b-form__field f-radio-group">';

    for (var i = 0; i < periodicItems.length; i++) {
        var stateOfRadio2 = ( (routes[index].hasOwnProperty("periodic")) && (routes[index].periodic == periodicItems[i].value) ? " state-active" : "");
        routesOptionsHtml += '<div class="b-form__radio"><div class="f-radio' + stateOfRadio2 + '">';
        routesOptionsHtml += '<input type="radio" name="periodic" onclick="periodicClickHandler(' + index + ', \'' + periodicItems[i].value + '\');" value="' + periodicItems[i].value + '" id="radio_2_' + i + '" /></div>';
        routesOptionsHtml += '<label for="radio_2_' + i + '" class="b-form__label-radio">' + periodicItems[i].name + '</label></div>';
    }

    routesOptionsHtml += '</div></div></div>';
    routesOptionsHtml += '</div>';

    routesOptionsHtml += '<div class="g-clear"></div>';
    routesOptionsHtml += '            <input type="button" value="Убрать это перемещение" class="f-button" onclick="removeRoute(' + index + '); return false;" />';

    $('#routesOptionsContainer').html(routesOptionsHtml);

    $('.f-radio').on('change', 'input', function () {
        var radio = $(this).closest('.f-radio');
        if (radio.hasClass('state-active')) {
            radio.removeClass('state-active');
        }
        else {
            $(this).closest('.f-radio-group').find('.f-radio').removeClass('state-active');
            radio.addClass('state-active');
        }
    });
}

function addRouteHandler() {
    if ($("#addRouteButton").val() == "добавить перемещение") {
        $("#addRouteButton").val("отменить добавление");

        saveCurrentRouteGeometry();
        currentRouteIndex = -1;
        $('#routesOptionsContainer').html("");

        getMovie().clearLocalLayer(POINTS_LAYER_ID);

        createPointsMarkers();

        var minX, maxX, minY, maxY;
        for (var i = 0; i < points.length; i++) {
            minX = Math.min(minX, points[i].x1) || points[i].x1;
            maxX = Math.max(maxX, points[i].x2) || points[i].x2;
            minY = Math.min(minY, points[i].y1) || points[i].y1;
            maxY = Math.max(maxY, points[i].y2) || points[i].y2;
        }

        var geometry = [{"x": (minX + maxX) / 2, "y": (minY + maxY) / 2}];
        geometry = JSON.parse(getMovie().transformCoordinates(JSON.stringify(geometry), "WGS84", "Samara"));

        getMovie().setCenter(geometry[0].y, geometry[0].x);
        getMovie().slideToScale(6, 10);

        fromPointIndex = -1;
        toPointIndex = -1;

        getMovie().addListener("MARKER_CLICK", "clickFirstPointHandler");

        $('#selectingRoutePointsContainer').html("<p>Нажмите на место, откуда начинается перемещение.</p>");
    }
    else {
        stopAllAddRouteActions();
    }
}

function stopAllAddRouteActions() {
    $("#addRouteButton").val("добавить перемещение");
    $('#selectingRoutePointsContainer').html("");
    getMovie().removeListener("MARKER_CLICK", "clickFirstPointHandler");
    getMovie().removeListener("MARKER_CLICK", "clickSecondPointHandler");
}


function clickFirstPointHandler(responce) {
    getMovie().removeListener("MARKER_CLICK", "clickFirstPointHandler");

    var clickMarkerResult = JSON.parse(responce);

    var markerInfo = JSON.parse(getMovie().getMarkerInfo(clickMarkerResult.markerId));
    fromPointIndex = markerInfo.description;

    var selectingHtml = "<p>Откуда - " + points[fromPointIndex].name + "</p>";
    selectingHtml = selectingHtml + "<p>Нажмите на место, куда совершается перемещение.</p>";

    $('#selectingRoutePointsContainer').html(selectingHtml);

    getMovie().addListener("MARKER_CLICK", "clickSecondPointHandler");
}

function clickSecondPointHandler(responce) {
    stopAllAddRouteActions();

    var clickMarkerResult = JSON.parse(responce);
    var markerInfo = JSON.parse(getMovie().getMarkerInfo(clickMarkerResult.markerId));
    toPointIndex = markerInfo.description;

    if ((fromPointIndex >= 0) && (toPointIndex >= 0) && (fromPointIndex != toPointIndex)) {
        routes.push({
            "pointFromIndex": fromPointIndex,
            "pointToIndex": toPointIndex,
            "geometry": "",
            "weight": 3
        });
        updateRoutesList();

        showForRoute(routes.length - 1);
    }
}

function removeRoute(index) {
    routes.splice(index, 1);
    updateRoutesList();
    currentRouteIndex = -1;
    $('#routesOptionsContainer').html("");
    getMovie().clearLocalLayer(POINTS_LAYER_ID);
}

function typeButtonHandler(index, typeOfPoint, nameOfPoint) {
    points[index].type = typeOfPoint;
    points[index].name = nameOfPoint;

    getMovie().setMarkerProperty(points[index].markerId, "name", nameOfPoint);

    if (typeOfPoint == "remove") {
        getMovie().addListener("MARKER_LOCK", "lockForDeleteHandler");
        getMovie().lockMarker(points[index].markerId);

        points.splice(index, 1);
        $('#pointsTypeButtonsContainer').html("");
        $('#pointsOptionsContainer').html("");
    }

    updatePointsList();
}

function moveKindsCheckHandler(index, value) {
    var variants = [];
    if (routes[index].hasOwnProperty("moveKinds")) {
        variants = routes[index].moveKinds.split(",");
    }
    if (variants.indexOf(value) >= 0) {
        variants.splice(variants.indexOf(value), 1);
    }
    else {
        variants.push(value);
    }
    routes[index].moveKinds = variants.join(",");
}

function movingTimeChangeHandler(index) {
    routes[index].movingTime = $("#movingTime").val();
}

function periodicClickHandler(index, value) {
    routes[index].periodic = value;
}

function lockForDeleteHandler(responce) {
    getMovie().removeListener("MARKER_LOCK", "lockForDeleteHandler");
    var lockedMarkersResult = JSON.parse(responce);
    getMovie().deleteMarker(lockedMarkersResult.markerId);
}

function postRoutesJson() {
    saveCurrentRouteGeometry();

    var somethingUnclassified = false;
    for (var i = 0; i < routes.length; i++) {
        somethingUnclassified = somethingUnclassified || (!routes[i].hasOwnProperty("moveKinds"));
    }
    if (somethingUnclassified) {
        alert("Есть еще непонятные перемещения. Уточните способы, которыми вы их совершаете, или удалите лишние.");
        return false;
    }
    else {
        // здесь будет аяксовая отправка этого JSON на сервер и потом переход на следующую страницу
        // console.log(JSON.stringify(routes));
        document.getElementById('routesForm:routes').value = JSON.stringify(routes);
    }
    return true;
}