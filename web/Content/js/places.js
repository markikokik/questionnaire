var map = new GeosamaraMap();
map.domain = "http://map.samadm.ru";
map.page = "/samara/";
map.modules = "Map,Toolbar,Messenger";
map.instruments = "ScaleInstrument,MoveInstrument,JSIntegration";
map.wms = "/wms1";
map.wfs = "/wfs1";
map.settings = "/settings1";
map.layers = "SM1,SM2,SM3,SM4";
map.scalesList = "200000,100000,50000,20000,10000,5000,2000,1000";
map.scale = 3;
map.x = 420;
map.y = 420;
map.selected = "";
map.insert("mapContainer");

var POINTS_LAYER_ID = "POINTS_LAYER_ID";
var POINTS_STYLE_ID = "distanceCheckpoint";

var points = [];

function getMovie() {
    if (typeof  window.gpClient != 'undefined') {
        return window.gpClient.getMovieAPI();
    } else {
        var M$ = navigator.appName.indexOf("Microsoft") != -1;
        return (M$ ? window : document)["GeoportalMap"];
    }
}

function imReady() {
    getMovie().createLocalLayer(POINTS_LAYER_ID, 'места, ЦПН');

    createPointsMarkers();
    if (points.length > 0) {
        showForPoint(0);
    }
}

function createPointsMarkers() {
    getMovie().clearLocalLayer(POINTS_LAYER_ID);

    for (var i = 0; i < points.length; i++) {
        getMovie().addListener("MARKER_CREATION", "createPointsMarkersCompleteHandler");
        var geometry = [{"x": (points[i].x1 + points[i].x2) / 2, "y": (points[i].y1 + points[i].y2) / 2}];

        geometry = JSON.parse(getMovie().transformCoordinates(JSON.stringify(geometry), "WGS84", "Samara"));
        for (var j = 0; j < geometry.length; j++) {
            var t = geometry[j].x;
            geometry[j].x = geometry[j].y;
            geometry[j].y = t;
        }

        getMovie().createMarker(POINTS_LAYER_ID, POINTS_STYLE_ID, JSON.stringify(geometry), points[i].name, i);
    }
}

function createPointsMarkersCompleteHandler(responce) {
    getMovie().removeListener("MARKER_CREATION", "createPointsMarkersCompleteHandler");
    var createdMarkersResult = JSON.parse(responce);
    var markerInfo = JSON.parse(getMovie().getMarkerInfo(createdMarkersResult.markerId));
    points[markerInfo.description].markerId = createdMarkersResult.markerId;
}


function updatePointsList() {
    var pointsListHtml = "";
    for (var i = 0; i < points.length; i++) {
        pointsListHtml += "<p><a href='#' onclick='showForPoint(" + i + "); return false;'>" + points[i].name + "</a></p>";
    }
    $('#pointsList').html(pointsListHtml);
}

function showForPoint(index) {
    if (points[index].hasOwnProperty("markerId")) {
        getMovie().moveToMarker(points[index].markerId, true);

        var buttonsListHtml = "";
        buttonsListHtml += '<div class="b-form b-properties_form">';
        buttonsListHtml += '<div class="b-form__row">';
        buttonsListHtml += '<div class="b-form__right_col">';
        buttonsListHtml += '<div class="b-form__field f-radio-group">';

        var buttonsItems = [];
        buttonsItems.push({"name": "дом", "value": "home"});
        buttonsItems.push({"name": "работа", "value": "work"});
        buttonsItems.push({"name": "дом родни", "value": "home2"});
        buttonsItems.push({"name": "магазин", "value": "shop"});
        buttonsItems.push({"name": "детская площадка", "value": "childplace"});
        buttonsItems.push({"name": "другое", "value": "another"});
        buttonsItems.push({"name": "это ничем не примечательное место, убрать", "value": "remove"});

        for (var i = 0; i < buttonsItems.length; i++) {
            var stateOfRadio = ( (points[index].hasOwnProperty("type")) && (points[index].type == buttonsItems[i].value) ? " state-active" : "");
            buttonsListHtml += '<div class="b-form__radio"><div class="f-radio' + stateOfRadio + '">';
            buttonsListHtml += '<input type="radio" name="type" onclick="typeButtonHandler(' + index + ', \'' + buttonsItems[i].value + '\', \'' + buttonsItems[i].name + '\');" value="' + buttonsItems[i].value + '" id="radio_1_' + i + '" /></div>';
            buttonsListHtml += '<label for="radio_1_' + i + '" class="b-form__label-radio">' + buttonsItems[i].name + '</label></div>';
        }
        buttonsListHtml += "</div></div></div></div>";

        $('#pointsTypeButtonsContainer').html(buttonsListHtml);


        var usualTimeItems = [];
        usualTimeItems.push({"name": "утром", "value": "morning"});
        usualTimeItems.push({"name": "днем", "value": "day"});
        usualTimeItems.push({"name": "вечером", "value": "evening"});
        usualTimeItems.push({"name": "ночью", "value": "night"});

        var pointsOptionsHtml = "";
        pointsOptionsHtml += '<div class="b-form b-properties_form">';
        pointsOptionsHtml += '	<div class="b-form__row">';
        pointsOptionsHtml += '		<div class="b-form__left_col">Обычно бываю здесь</div>';
        pointsOptionsHtml += '		<div class="b-form__right_col">';
        for (var i = 0; i < usualTimeItems.length; i++) {
            var stateOfCheck = ""
            if (points[index].hasOwnProperty("usualTime")) {
                var variants = points[index].usualTime.split(",");
                if (variants.indexOf(usualTimeItems[i].value) >= 0) {
                    stateOfCheck = 'checked="checked"';
                }
            }
            pointsOptionsHtml += '			<div class="checkbox"><span class="ui-custom-checkbox">';
            pointsOptionsHtml += '				<input type="checkbox" ' + stateOfCheck + ' " name="usualTime_' + i + '" id="usualTime_' + i + '" onclick="usualTimeCheckHandler(' + index + ', \'' + usualTimeItems[i].value + '\');" /></span>';
            pointsOptionsHtml += '				<label for="usualTime_' + i + '">' + usualTimeItems[i].name + '</label></div>';
        }
        pointsOptionsHtml += '		</div></div>';


        var dailyTime = ( points[index].hasOwnProperty("dailyTime") ? points[index].dailyTime : '');

        pointsOptionsHtml += '<div class="b-form__row">';
        pointsOptionsHtml += '<div class="b-form__left_col">В среднем я бываю здесь</div>';
        pointsOptionsHtml += '<div class="b-form__right_col">';
        pointsOptionsHtml += '<div class="b-form__field">';
        pointsOptionsHtml += '<input type="text" name="dailyTime" id="dailyTime" class="f-input-text" value="' + dailyTime + '" placeholder="5" onchange="dailyTimeChangeHandler(' + index + ')"  style="width: 100px;" />&nbsp;часов в сутки';
        pointsOptionsHtml += '</div></div></div>';

        pointsOptionsHtml += '<div class="g-clear"></div>';

        var likeThisPlaceItems = [];
        likeThisPlaceItems.push({"name": "нравится", "value": "like"});
        likeThisPlaceItems.push({"name": "не нравится", "value": "unlike"});

        pointsOptionsHtml += '<div class="b-form__row">';
        pointsOptionsHtml += '<div class="b-form__left_col">Мне это место</div>';
        pointsOptionsHtml += '<div class="b-form__right_col">';
        pointsOptionsHtml += '<div class="b-form__field f-radio-group">';

        for (var i = 0; i < likeThisPlaceItems.length; i++) {
            var stateOfRadio2 = ( (points[index].hasOwnProperty("likeThisPlace")) && (points[index].likeThisPlace == likeThisPlaceItems[i].value) ? " state-active" : "");
            pointsOptionsHtml += '<div class="b-form__radio"><div class="f-radio' + stateOfRadio2 + '">';
            pointsOptionsHtml += '<input type="radio" name="likeThisPlace" onclick="likeThisPlaceClickHandler(' + index + ', \'' + likeThisPlaceItems[i].value + '\');" value="' + likeThisPlaceItems[i].value + '" id="radio_2_' + i + '" /></div>';
            pointsOptionsHtml += '<label for="radio_2_' + i + '" class="b-form__label-radio">' + likeThisPlaceItems[i].name + '</label></div>';
        }

        pointsOptionsHtml += '</div></div></div>';
        pointsOptionsHtml += '</div>';

        $('#pointsOptionsContainer').html(pointsOptionsHtml);

        $('.f-radio').on('change', 'input', function () {

            var radio = $(this).closest('.f-radio');

            if (radio.hasClass('state-active')) {
                radio.removeClass('state-active');
            }
            else {
                $(this).closest('.f-radio-group').find('.f-radio').removeClass('state-active');
                radio.addClass('state-active');
            }

        });
    }
    else {
        $('#pointsTypeButtonsContainer').html("Дождитесь запуска карты");
    }
}

function addPointHandler() {
    if ($("#addPointButton").val() == "добавить место") {
        $("#addPointButton").val("отменить добавление");

        getMovie().addListener("MAP_CLICK", "clickMapForCreatePointHandler");
    }
    else {
        $("#addPointButton").val("добавить место");

        getMovie().removeListener("MAP_CLICK", "clickMapForCreatePointHandler");
    }
}

function clickMapForCreatePointHandler(responce) {
    $("#addPointButton").val("добавить место");
    getMovie().removeListener("MAP_CLICK", "clickMapForCreatePointHandler");
    var clickMapResult = JSON.parse(responce);

    var geometry = [{"x": clickMapResult.y - 50, "y": clickMapResult.x - 50}, {
        "x": clickMapResult.y + 50,
        "y": clickMapResult.x + 50
    }];
    geometry = JSON.parse(getMovie().transformCoordinates(JSON.stringify(geometry), "Samara", "WGS84"));

    var newPoint =
        {
            "name": "Непонятное место " + (points.length + 1),
            "x1": geometry[0].x,
            "y1": geometry[0].y,
            "x2": geometry[1].x,
            "y2": geometry[1].y,
            "weight": 5
        };
    points.push(newPoint);

    getMovie().addListener("MARKER_CREATION", "createPointsMarkersCompleteHandler");
    var geometry = [{"x": clickMapResult.x - 50, "y": clickMapResult.y - 50}, {
        "x": clickMapResult.x - 50,
        "y": clickMapResult.y + 50
    }, {"x": clickMapResult.x + 50, "y": clickMapResult.y + 50}, {
        "x": clickMapResult.x + 50,
        "y": clickMapResult.y - 50
    }, {"x": clickMapResult.x - 50, "y": clickMapResult.y - 50}];

    getMovie().createMarker(POINTS_LAYER_ID, POINTS_STYLE_ID, JSON.stringify(geometry), newPoint.name, points.length - 1);

    updatePointsList();
}

function typeButtonHandler(index, typeOfPoint, nameOfPoint) {
    points[index].type = typeOfPoint;
    points[index].name = nameOfPoint;

    getMovie().setMarkerProperty(points[index].markerId, "name", nameOfPoint);

    if (typeOfPoint == "remove") {
        getMovie().addListener("MARKER_LOCK", "lockForDeleteHandler");
        getMovie().lockMarker(points[index].markerId);

        points.splice(index, 1);
        $('#pointsTypeButtonsContainer').html("");
        $('#pointsOptionsContainer').html("");
    }

    updatePointsList();
}

function usualTimeCheckHandler(index, value) {
    var variants = [];
    if (points[index].hasOwnProperty("usualTime")) {
        variants = points[index].usualTime.split(",");
    }
    if (variants.indexOf(value) >= 0) {
        variants.splice(variants.indexOf(value), 1);
    }
    else {
        variants.push(value);
    }
    points[index].usualTime = variants.join(",");
}

function dailyTimeChangeHandler(index) {
    points[index].dailyTime = $("#dailyTime").val();
}

function likeThisPlaceClickHandler(index, value) {
    points[index].likeThisPlace = value;
}

function lockForDeleteHandler(responce) {
    getMovie().removeListener("MARKER_LOCK", "lockForDeleteHandler");
    var lockedMarkersResult = JSON.parse(responce);
    getMovie().deleteMarker(lockedMarkersResult.markerId);
}

function postPointsJson() {
    var somethingUnclassified = false;
    for (var i = 0; i < points.length; i++) {
        somethingUnclassified = somethingUnclassified || (!points[i].hasOwnProperty("type"));
    }
    // if (somethingUnclassified) {
    //     alert("Есть еще непонятные места. Уточните их назначение или удалите лишние.");
    //     return false;
    // } else {
    // здесь будет аяксовая отправка этого JSON на сервер и потом переход на следующую страницу
    // console.log(JSON.stringify(points));
    document.getElementById('pointForm:points').value = JSON.stringify(points);
    // }
    return true;
}